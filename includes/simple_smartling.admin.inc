<?php

/**
 * @file
 * Admin page callbacks for simple_smartling.module.
 */

/**
 * Page callback for the admin settings form.
 *
 * @return array
 *   The settings form array.
 *
 * @see simple_smartling_menu()
 */
function simple_smartling_settings_form() {
  $form['simple_smartling_alter_admin_forms'] = array(
    '#title' => t('Alter admin forms'),
    '#description' => t('Check this to enable Smartling on Drupal administration forms.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('simple_smartling_alter_admin_forms', 0),
  );

  $form['simple_smartling_no_translate_node_types'] = array(
    '#title' => t('Non-translatable node types'),
    '#description' => t('Disable translation on these node types.'),
    '#tree' => TRUE,
    '#type' => 'fieldset',
  );

  _simple_smartling_add_non_translatable_node_types($form);
  _simple_smartling_add_non_translatable_paths($form);

  return system_settings_form($form);
}

/**
 * Add settings for non-translatable node types.
 *
 * @param array $form
 *   The form array, passed by reference.
 */
function _simple_smartling_add_non_translatable_node_types(array &$form) {
  $defaults = variable_get('simple_smartling_no_translate_node_types');

  // Sort the node types alphabetically by name.
  $types = node_type_get_types();
  uksort($types, function ($a, $b) {
    return $a > $b;
  });

  foreach ($types as $type) {
    $form['simple_smartling_no_translate_node_types'][$type->type] = array(
      '#title' => $type->name,
      '#type' => 'checkbox',
      '#default_value' => isset($defaults[$type->type]) ? $defaults[$type->type] : FALSE,
    );
  }
}

/**
 * Add settings for non-translatable node types.
 *
 * @param array $form
 *   The form array, passed by reference.
 */
function _simple_smartling_add_non_translatable_paths(array &$form) {
  $form['simple_smartling_no_translate_path'] = array(
    '#title' => t('Non-translatable paths'),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array(
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>',
    )),
    '#type' => 'textarea',
    '#default_value' => variable_get('simple_smartling_no_translate_path'),
  );
}
