CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Simple Smartling module provides a method to integrate with the Smartling
global delivery network (https://help.smartling.com/hc/en-us/categories/360000195273-Global-Delivery-Network).

If you are using another Smartling product, you may need to instead use the
Smartling Connector module (https://www.drupal.org/project/smartling).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/simple_smartling

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/simple_smartling


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Administer Simple Smartling

     Administer the Simple Smartling module settings.

 * Configure the module settings in Administration » Configuration »
   Regional and language Administration » Simple Smartling settings


MAINTAINERS
-----------

Current maintainers:
 * Oliver Davies (opdavies) - https://www.drupal.org/u/opdavies

This project has been sponsored by:
 * Microserve
   UK Drupal Experts. Visit https://microserve.io for more information.
