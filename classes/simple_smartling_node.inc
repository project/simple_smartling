<?php

/**
 * Perform changes to nodes.
 */
class SimpleSmartlingNode {

  /**
   * Add non-translatable classes to a node, if applicable.
   *
   * @param array $variables
   *   The variables array from simple_smartling_preprocess_page(), passed by
   *   reference.
   *
   * @see simple_smartling_preprocess_page()
   */
  public static function addNonTranslatableClasses(array &$variables) {
    if (!array_key_exists('node', $variables)) {
      return;
    }

    if (self::nodeTypeIsNonTranslatable($variables['node'])) {
      $variables['content_attributes_array']['class'][] = SIMPLE_SMARTLING_NO_TRANSLATE_CLASS;
    }
  }

  /**
   * Determine whether a node is of an untranslatable type.
   *
   * @param \stdClass $node
   *   A node.
   *
   * @return bool
   *   TRUE if the node type is not translatable. Otherwise FALSE.
   */
  protected static function nodeTypeIsNonTranslatable(\stdClass $node) {
    $node_types = variable_get('simple_smartling_no_translate_node_types', array());

    if (!array_key_exists($node->type, $node_types)) {
      return FALSE;
    }

    return $node_types[$node->type] == TRUE;
  }

}
