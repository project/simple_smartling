<?php

/**
 * Perform alterations to forms.
 */
class SimpleSmartlingForm {

  /**
   * Alter the form.
   *
   * @param array $form
   *   The form array, passed by reference.
   */
  public static function alterForm(array &$form) {
    $alter_admin_forms = variable_get('simple_smartling_alter_admin_forms', 0) === 1;
    if (!self::shouldAlterForm(drupal_get_path_alias(), $alter_admin_forms)) {
      return;
    }

    $form['actions']['submit']['#attributes']['class'][] = SIMPLE_SMARTLING_TRANSLATE_CLASS;
  }

  /**
   * Determine if the form should be altered.
   *
   * @param string $path
   *   The path to check.
   * @param bool $alter_admin_forms
   *   Whether to alter forms on admin paths.
   *
   * @return bool
   *   TRUE if the form should be altered, otherwise FALSE.
   */
  public static function shouldAlterForm($path, $alter_admin_forms = FALSE) {
    if (!$alter_admin_forms) {
      return !path_is_admin($path);
    }

    return TRUE;
  }

}
