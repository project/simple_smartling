<?php

/**
 * Perform changes based on path.
 */
class SimpleSmartlingPath {

  /**
   * Add classes for non-translatable paths.
   *
   * @param array $variables
   *   The variables array.
   * @param string $path
   *   The path to check.
   *
   * @see simple_smartling_preprocess_page()
   */
  public static function addNonTranslatableClasses(array &$variables, $path) {
    if (self::isNonTranslatablePath($path)) {
      $variables['content_attributes_array']['class'][] = SIMPLE_SMARTLING_NO_TRANSLATE_CLASS;
    }
  }

  /**
   * Determine if a path is non-translatable.
   *
   * @param string $path
   *   The path to check.
   *
   * @return bool
   *   TRUE if the path is non-translatable. Otherwise FALSE.
   */
  protected static function isNonTranslatablePath($path) {
    return drupal_match_path($path, variable_get('simple_smartling_no_translate_path'));
  }

}
